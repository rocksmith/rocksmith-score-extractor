call pp rocksmith_score_extractor.pl -o bin/rocksmith_score_extractor.exe

cd rocksmith-psarc-extractor
call build.bat

cd ..\rocksmith-user-data-extractor
call build.bat

cd ..\bin
mkdir rocksmith-psarc-extractor
mkdir rocksmith-user-data-extractor

cd ..
move rocksmith-psarc-extractor\bin\* bin\rocksmith-psarc-extractor
move rocksmith-user-data-extractor\bin\* bin\rocksmith-user-data-extractor