# Usage

Run the "exe" file and select the proper Steam/Rocksmith profiles. Then select the DLC files you would like to export score attack scores for.

```
rocksmith_score_extractor.exe
```

# Build Requirements

Install Strawberry Perl (https://strawberryperl.com/)

Install Tk and PAR::Packer from CPAN

```
perl -MCPAN -e shell
install Tk
install Par::Packer
```

# Build Instructions

Run the following command to build the application

```
build.bat
```
