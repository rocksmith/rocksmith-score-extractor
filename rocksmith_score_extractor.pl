use Digest::MD5 qw(md5_hex);
use File::Basename;
use File::Find;
use File::Slurp;
use JSON;
use strict;
use Tk;
use Tk::Dialog;
use warnings;

# Use main window as a global object so we can spaw popups as needed
my $MW = Tk::MainWindow->new();
$MW->withdraw();

# Define path required executables
my $ROCKSMITH_PSARC_EXTRACTOR_EXE = './rocksmith-psarc-extractor/rocksmith_psarc_extractor.exe';
my $ROCKSMITH_USER_DATA_EXTRACTOR_EXE = './rocksmith-user-data-extractor/rocksmith_user_data_extractor.exe';

# Define Rocksmith app ID
my $ROCKSMITH_APP_ID = 221680;

my $CACHE_DIR = 'cache';
mkdir $CACHE_DIR;

main();

sub main {
    my $output_file = 'rocksmith_score_export.json';

    my $profile_data = parse_rocksmith_profile();

    my $psarc_data = parse_psarc_files();

    my $score_attack_data = merge_profile_and_psarc_data($profile_data, $psarc_data);

    # Generate a hash from the JSON output to prevent people from mucking with scores
    my $hash = md5_hex(encode_json($score_attack_data->{'songs'}));

    $score_attack_data->{'hash'} = $hash;

    my $json_output = encode_json($score_attack_data);

    print "Writing output to $output_file.\n";
    write_file($output_file, $json_output);
}

sub parse_rocksmith_profile {
    my $profile_datafile;

    if (-f $CACHE_DIR.'/profile_cache.dat') {
        $profile_datafile = read_file($CACHE_DIR.'/profile_cache.dat');
    }
    else {
        # Get the Steam userdata folder
        my $userdata_folder = get_steam_userdata_folder();

        # Get the Rocksmith data folder
        my $rocksmith_data_folder = get_rocksmith_data_folder($userdata_folder);

        # Get the associated rocksmith profile
        my $rocksmith_profile = get_rocksmith_profile($rocksmith_data_folder);
        
        $profile_datafile = $rocksmith_data_folder.'/'.$rocksmith_profile.'_PRFLDB';

        # Cache the filename for future runs
        write_file($CACHE_DIR.'/profile_cache.dat', $profile_datafile);
    }

    my $profile_data = extract_profile_score_attack_data($profile_datafile);

    return $profile_data;
}

sub parse_psarc_files {
    my $rocksmith_folder;
    my $rocksmith_dlc_folder;

    if (-f $CACHE_DIR.'/rocksmith_location.dat') {
        $rocksmith_folder = read_file($CACHE_DIR.'/rocksmith_location.dat');
    }
    else {
        $rocksmith_folder = get_rocksmith_folder();

        write_file($CACHE_DIR.'/rocksmith_location.dat', $rocksmith_folder);
    }

    if (-f $CACHE_DIR.'/rocksmith_dlc_location.dat') {
        $rocksmith_dlc_folder = read_file($CACHE_DIR.'/rocksmith_dlc_location.dat');
    }
    else {
        # Get the Rocksmith DLC folder
        $rocksmith_dlc_folder = get_rocksmith_dlc_folder();

        write_file($CACHE_DIR.'/rocksmith_dlc_location.dat', $rocksmith_dlc_folder);
    }

    my $psarc_files = select_psarc_files($rocksmith_folder, $rocksmith_dlc_folder);

    my $psarc_data = extract_psarc_data($psarc_files);

    return $psarc_data;
}

# Gets the Steam userdata folder.
# Allows user to manually select it if it is not in the default location
sub get_steam_userdata_folder {
    my $userdata_folder = 'C:/Program Files (x86)/Steam/userdata';
    if (!-d $userdata_folder) {
        print "\nSteam userdata folder not found. Please select your userdata folder.\n"
            . "The default location is '$userdata_folder'\n";

        $userdata_folder = $MW->chooseDirectory(-title => 'Select Steam userdata folder');
        if (!-d $userdata_folder) {
            die "\nInvalid userdata folder provided.\n";
        }
    }

    return $userdata_folder;
}

# Gets the Rocksmith main folder
# Allows user to manually select it if it is not the default location
sub get_rocksmith_folder {
    my $rocksmith_folder = 'C:/Program Files (x86)/Steam/steamapps/common/Rocksmith2014';
    if (!-d $rocksmith_folder) {
        print "\nRocksmith folder not found. Please select your Rocksmith folder.\n"
            . "The default location is '$rocksmith_folder'\n";

        $rocksmith_folder = $MW->chooseDirectory(-title => 'Select Rocksmith folder');
        if (!-d $rocksmith_folder) {
            die "\nInvalid Rocksmith folder provided.\n";
        }
    }

    return $rocksmith_folder;
}

# Gets the Rocksmith dlc folder.
# Allows user to manually select it if it is not in the default location
sub get_rocksmith_dlc_folder {
    my $rocksmith_dlc_folder = 'C:/Program Files (x86)/Steam/steamapps/common/Rocksmith2014/dlc';
    if (!-d $rocksmith_dlc_folder) {
        print "\nRocksmith dlc folder not found. Please select your Rocksmith dlc folder.\n"
            . "The default location is '$rocksmith_dlc_folder'\n";

        $rocksmith_dlc_folder = $MW->chooseDirectory(-title => 'Select Rocksmith dlc folder');
        if (!-d $rocksmith_dlc_folder) {
            die "\nInvalid Rocksmith dlc folder provided.\n";
        }
    }

    return $rocksmith_dlc_folder;
}

# Gets the Rocksmith data folder.
# Allows the user to manually specify their steam account if more than one is found.
sub get_rocksmith_data_folder {
    my $userdata_folder = shift;

    # Select the correct Steam profile
    opendir my $dh, $userdata_folder or die "\nFailed to open Steam userdata directory '$userdata_folder': $!\n";
    my @user_subfolders = grep !/^\.\.?$/, readdir $dh;
    closedir $dh;

    my $rocksmith_data_folder = $userdata_folder;
    if (!@user_subfolders) {
        die "No Steam userdata found.\n";
    }
    if (scalar @user_subfolders > 1) {
        my %accounts;
        for my $user_subfolder(@user_subfolders) {
            my $username = extract_username($userdata_folder.'/'.$user_subfolder.'/config/localconfig.vdf');
            $accounts{$username} = $user_subfolder;
        }

        print "\nMultiple Steam accounts found, please enter your steam username...\n"
            . "\n"
            . "Username: ";

        my $username = <>;
        chomp $username;

        if (!exists $accounts{$username}) {
            die "\nNo data found for user '$username'\n";
        }
        else {
            $rocksmith_data_folder .= '/'.$accounts{$username}.'/'.$ROCKSMITH_APP_ID.'/remote';
        }
    }
    else {
        $rocksmith_data_folder .= '/'.$user_subfolders[0].'/'.$ROCKSMITH_APP_ID.'/remote';
    }
}

# Gets the steam username from the provided localconfig.vdf file
sub extract_username {
    my $config_file = shift;

    my $content = read_file($config_file);

    if ($content =~ /"PersonaName"\s+"([^"]+)"/) {
        return $1;
    }
    else {
        die "\nInvalid format for config file '$config_file'\n";
    }
}

# Get the name of the Rocksmith profile you want to export scores from
# If more than one option is found, the user will be asked to enter their profile name
sub get_rocksmith_profile {
    my $rocksmith_data_folder = shift;

    my $local_profile_json = `$ROCKSMITH_USER_DATA_EXTRACTOR_EXE "$rocksmith_data_folder/LocalProfiles.json"`;

    my $local_profile_data = decode_json($local_profile_json);

    my %profiles;
    for my $profile(@{$local_profile_data->{'Profiles'}}) {
        $profiles{$profile->{'PlayerName'}} = $profile->{'UniqueID'};
    }

    my @profile_names = keys %profiles;
    if (!scalar @profile_names) {
        die "\nNo Rocksmith profiles found in Steam user data.\n";
    }
    if (scalar @profile_names > 1) {

        print "\nMultiple Rocksmith profiles found, please enter your Rocksmith profile name...\n"
            . "\n"
            . "Profile: ";

        my $profile_name = <>;
        chomp $profile_name;

        if (!exists $profiles{$profile_name}) {
            die "\nNo profile found for name '$profile_name'\n";
        }
        else {
            return $profiles{$profile_name};
        }
    }
    else {
        return $profiles{$profile_names[0]};
    }
}

# Extract song score data from the profile and store it in a hash
sub extract_profile_score_attack_data {
    my $profile_file = shift;

    my $profile_json =`$ROCKSMITH_USER_DATA_EXTRACTOR_EXE \"$profile_file\"`;

    my $profile_data = decode_json($profile_json);

    my %scores;
    for my $song_id(keys %{$profile_data->{'SongsSA'}}) {

        # Not bothering with easy/medium (there's no real competition here)
        # my $easy_score = $profile_data->{'SongsSA'}{$song_id}{'HighScores'}{'Easy'};
        # my $medium_score = $profile_data->{'SongsSA'}{$song_id}{'HighScores'}{'Medium'};
        my $hard_score = $profile_data->{'SongsSA'}{$song_id}{'HighScores'}{'Hard'};
        my $master_score = $profile_data->{'SongsSA'}{$song_id}{'HighScores'}{'Master'};

        # if ($easy_score) {
            # $scores{$song_id}{'scores'}{'easy'} = $easy_score;
        # }
        # if ($medium_score) {
            # $scores{$song_id}{'scores'}{'medium'} = $medium_score;
        # }
        if ($hard_score) {
            $scores{$song_id}{'scores'}{'hard'} = $hard_score;
        }
        if ($master_score) {
            $scores{$song_id}{'scores'}{'master'} = $master_score;
        }
    }

    return \%scores;
}

# Select PSARC files to process
sub select_psarc_files {
    my $rocksmith_folder = shift;
    my $rocksmith_dlc_folder = shift;

    print "\nWould you like to export all scores, or manually select specific songs?\n"
        . "\n"
        . "(a)ll (excludes on-disc songs and RS1 DLC)\n"
        . "(m)anual selection\n"
        . "\n"
        . "?: ";

    my $input = <>;
    chomp $input;

    my @files;
    if ($input =~ m/(a|all)/i) {
        find(sub {
            if ($_ =~ m/_p[.]psarc$/) {
                push @files, $File::Find::name;
            }
        }, $rocksmith_dlc_folder);

        # Also capture on disc songs
        if (-f $rocksmith_folder.'/songs.psarc') {
            push @files, $rocksmith_folder.'/songs.psarc';
        }
    }
    elsif ($input =~ m/(m|man|manual|manual selection)/i) {
        print "\nSelect the .psarc files you would like to extract score attack data for.\n";
        @files = $MW->getOpenFile(
            -multiple => 1,
            -initialdir => $rocksmith_dlc_folder,
        );
    }

    return \@files;
}

# Extract song info from the selected PSARC file(s)
sub extract_psarc_data {
    my $psarc_files = shift;

    my @required_attributes = qw(
        DLCKey
        SongKey
        ArtistName
        ArtistNameSort
        SongName
        SongNameSort
        AlbumName
        AlbumNameSort
        SongYear
        PathName
    );

    my %arrangements = (
        1 => 'Lead',
        2 => 'Rhythm',
        4 => 'Bass',
    );

    print "\nExtracting psarc data...\n";
    my %psarc_data;

    PSARC_LOOP:
    for my $file(@{$psarc_files}) {
        my ($basename, $path, $suffix) = fileparse($file, '.psarc');

        # Skip non .psarc files
        if ($suffix ne '.psarc') {
            next;
        }

        print "Processing '$file'.\n";

        # Extract the manifest from the psarc file
        my $manifest_json = `$ROCKSMITH_PSARC_EXTRACTOR_EXE "$file"`;
        my $manifest = decode_json($manifest_json);

        # As we process arrangements check for duplicates so we can resolve conflicts
        # Tracks with "alt" paths may have 2 lead or 2 rhythm arrangements. When this happens,
        # use the manifest URN number to resolve which path should be marked as "alt"
        my %seen_arrangements;

        # Stores song data for the current PSARC file. If an error occurs during processing the
        # PSARC file is skipped and all collected data from that PSARC file will be discarded
        my %song_data;

        # Loop through all arrangements in the manifest and store them in 
        ARRANGEMENT_LOOP:
        for my $arrangement_key(keys %{$manifest->{'Entries'}}) {
            my %attributes = %{$manifest->{'Entries'}{$arrangement_key}{'Attributes'}};

            # If the song is not DLC give it a blank DLC key
            if (exists $attributes{'DLC'} and !$attributes{'DLC'}) {
                $attributes{'DLCKey'} = "";
            }

            # Make sure all required attributes exist (If any are missing, skip this arrangement)
            for my $required_attribute(@required_attributes) {
                if (!exists $attributes{$required_attribute}) {
                    next ARRANGEMENT_LOOP;
                }
            }

            # Combine DLC key and song key to get a unique identifier for the song
            my $song_key = $attributes{'DLCKey'}.$attributes{'SongKey'};

            my $arrangement = $attributes{'PathName'};

            # If we find a duplicate path, abort parsing the PSARC
            if (exists $seen_arrangements{$song_key}{$arrangement}) {

                # For now we are ignoring bonus/alt paths. If we get here then we have a problem
                warn "ERROR: Duplicate arrangement found"
                   . "       Arrangement 1: '$arrangement_key'\n"
                   . "       Arrangement 2: '$seen_arrangements{$song_key}{$arrangement}\n"
                   . "       Skipping '$file'\n";
                next PSARC_LOOP;
            }

            # Extract song metadata
            if (!exists $song_data{'songs'}{$song_key}) {
                $song_data{'songs'}{$song_key} = {
                    'artist'      => $attributes{'ArtistName'},
                    'artistSort'  => $attributes{'ArtistNameSort'},
                    'title'       => $attributes{'SongName'},
                    'titleSort'   => $attributes{'SongNameSort'},
                    'album'       => $attributes{'AlbumName'},
                    'albumSort'   => $attributes{'AlbumNameSort'},
                    'year'        => $attributes{'SongYear'},
                };
            }

            $song_data{'songs'}{$song_key}{'arrangements'}{$arrangement_key}{'name'} = $arrangement;

            # Mark this arrangement as seen (store the URN string to resolve conflicts)
            $seen_arrangements{$song_key}{$arrangement} = $arrangement_key;
        }

        # Add the data to the main hash storing all PSARC data
        for my $song_key(keys %{$song_data{'songs'}}) {
            $psarc_data{'songs'}{$song_key} = $song_data{'songs'}{$song_key};
        }
    }

    return \%psarc_data;
}

sub merge_profile_and_psarc_data {
    my $profile_data = shift;
    my $psarc_data = shift;

    my %score_attack_data;
    for my $song_key(keys %{$psarc_data->{'songs'}}) {

        my $found_arrangement = 0;
        for my $arrangement_key(keys %{$psarc_data->{'songs'}{$song_key}{'arrangements'}}) {
            if (exists $profile_data->{$arrangement_key}) {
                $score_attack_data{'songs'}{$song_key}{'arrangements'}{$arrangement_key} = $psarc_data->{'songs'}{$song_key}{'arrangements'}{$arrangement_key};
                $score_attack_data{'songs'}{$song_key}{'arrangements'}{$arrangement_key}{'scores'} = $profile_data->{$arrangement_key}{'scores'};
                $found_arrangement = 1;
            }
        }

        if ($found_arrangement) {
            $score_attack_data{'songs'}{$song_key}{'artist'} = $psarc_data->{'songs'}{$song_key}{'artist'};
            $score_attack_data{'songs'}{$song_key}{'artistSort'} = $psarc_data->{'songs'}{$song_key}{'artistSort'};
            $score_attack_data{'songs'}{$song_key}{'title'} = $psarc_data->{'songs'}{$song_key}{'title'};
            $score_attack_data{'songs'}{$song_key}{'titleSort'} = $psarc_data->{'songs'}{$song_key}{'titleSort'};
            $score_attack_data{'songs'}{$song_key}{'album'} = $psarc_data->{'songs'}{$song_key}{'album'};
            $score_attack_data{'songs'}{$song_key}{'albumSort'} = $psarc_data->{'songs'}{$song_key}{'albumSort'};
            $score_attack_data{'songs'}{$song_key}{'year'} = $psarc_data->{'songs'}{$song_key}{'year'};
        }
    }

    return \%score_attack_data;
}